## ADF Errors { data-transition="zoom-in slide-out" }

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Summary

* Night effect
* Terrain effect
* Mountain effect
* Thunderstorm activity
* Co-channel Interference
* Coastal refraction
* Quadrantal error

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Night Effect

* The structure of the ionosphere changes at night
* Reflected sky waves arrive later to the receiver than the ground wave
* The result is a reduction in useful range of the NDB at night

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/ionosphere.png" alt="Ionosphere" width="550px"/>
</p>

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Terrain Effect

* The ground wave travels more efficiently through water
* Over land, the ground wave attenuates
* The result is a greater range of the NDB over water than land

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Mountain Effect

* The sky wave may be reflected by a nearby mountain
* The result is similar to night effect
* Mountain effect can be managed by climbing to a higher altitude

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/mountain-effect.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Mountain Effect" width="550px"/>
</p>

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Thunderstorm Activity

* Lightning transmits radio waves at a similar frequency to the NDB
* The ADF indication swings between the last lightning location and the NDB
* The ADF becomes unusable

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/lightning.png" alt="Lightning" width="550px"/>
</p>

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Co-channel Interference

* Interference caused by a station transmitting on the same or similar frequency to the NDB
* The ADF indication will be random

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Coastal Refraction

* When a ground wave crosses the coast at 90&deg;, it continues in the same direction
* When a ground wave crosses the coast otherwise, it bends _(refracts)_ towards the coast line

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/coastal-refraction.png" alt="Coastal Refraction" width="550px"/>
</p>

---

## ADF Errors { data-transition="slide-in slide-out" }

###### Quadrantal Error

* The radio signal is distorted when it encounters the aircraft structure
* The ADF indicates the direction of the signal as it arrives at the antenna
* This effect is most common on bearings which are multiples of 45&deg;

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/quadrantal-error.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Quadrantal Error" width="400px"/>
</p>

---

