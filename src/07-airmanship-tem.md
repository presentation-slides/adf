## Airmanship/TEM/HF { data-transition="convex-in slide-out" }

<section>
  <table>
      <thead>
        <tr style="font-size: xx-large">
            <th>Threat</th>
            <th>Error</th>
            <th>Undesired State</th>
            <th>Mitigation</th>
        </tr>
      </thead>
      <tbody>
        <tr style="font-size: x-large">
          <td>Fatigue</td>
          <td>Failure to scan instruments</td>
          <td>Unusual Attitude</td>
          <td>I.M.S.A.F.E.</td>
        </tr>
        <tr style="font-size: x-large">
          <td>Misidentified Unusual Attitude</td>
          <td>Inappropriate pilot inputs</td>
          <td>Unusual Attitude</td>
          <td>Recognise symptoms of unusual attitude</td>
        </tr>
        <tr style="font-size: x-large">
          <td>Traffic conflict</td>
          <td>Loss of situational awareness</td>
          <td>Airprox, collision</td>
          <td>Lookout scan, Listening</td>
        </tr>
        <tr style="font-size: x-large">
          <td>IMC</td>
          <td>Unanticipated weather</td>
          <td>C.F.I.T.</td>
          <td>Obtain current and complete weather forecast</td>
        </tr>
      </tbody>
    </table>
</section>

---

