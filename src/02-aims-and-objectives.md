## Aims and Objectives { data-transition="zoom-in slide-out" }

###### Aims { data-transition="fade-in slide-out" }

* understand the principles of how an ADF works
* understand the principles of ADF operation
* understand the principles of ADF orientation
* understand the principles of track intercepts using the ADF

---

## Aims and Objectives { data-transition="fade-in slide-out" }

###### Objectives { data-transition="fade-in slide-out" }

* explain the workflow associated with setting up the ADF
* recall three errors experienced by the ADF
* explain the five ADF navigation aid interception questions

---

