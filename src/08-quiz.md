## Quiz on Objectives { data-transition="convex-in slide-out" }

* What are the three steps to setting up an ADF before using?
* What are three of the possible errors experienced by an ADF?
* What are the five questions to ask for navigation aid interception?
