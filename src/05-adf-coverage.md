## ADF Coverage { data-transition="zoom-in slide-out" }

---

## ADF Coverage { data-transition="slide-in slide-out" }

###### ADF Rated Coverage

* The rated coverage of each NDB depends on:
  * terrain between aircraft and NDB station
  * day or night
  * transmitter power

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/aip-bromelton-ndb.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="AIP Bromelton NDB" width="750px"/>
</p>

---

## ADF Coverage { data-transition="slide-in slide-out" }

###### ADF Broadcast Stations

* Until 2020, broadcasting radio stations and their frequency were published in the ERSA
* The location, identifier, power output and frequency of the station were provided
* A pilot was able to listen to the cricket on ABC by tuning the ADF to the correct frequency
* AirServices no longer publishes information on these stations
* Today they can be found on the ACMA website

---

