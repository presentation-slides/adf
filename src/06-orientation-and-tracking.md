## Orientation and Tracking { data-transition="zoom-in slide-out" }

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### ADF Setup

###### T.I.T.

* <span class="fragment highlight-red"><strong>T</strong></span>une: _set the correct NDB frequency on the ADF_
* <span class="fragment highlight-red"><strong>I</strong></span>dentify: _listen for the NDB morse identifier_
* <span class="fragment highlight-red"><strong>T</strong></span>est: _test the ADF is functioning correctly_

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/adf-and-radio.png" alt="ADF and Radio" width="350px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Fixed Card ADF

* **Relative Bearing**: the direction towards the NDB, relative to your heading
* <code><strong>0</strong></code>: the nose of the aircraft
* <code><strong>9</strong></code>: the right wing of the aircraft
* <code><strong>18</strong></code>: the tail of the aircraft
* <code><strong>27</strong></code>: the left wing of the aircraft

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/adf-fixed-card.png" alt="ADF Fixed Card" width="570px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Orientation

* Transpose the orientation of the ADF needle on to the Heading Indicator (HI)
* Imagine:
  * centre of HI is the NDB station
  * aircraft is at the base of HI
  * track to NDB is at the head of HI

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Orientation

* To begin orientating ourselves, we can ask five questions
  1. Where am I?
  2. Where do I want to be?
  3. Which way do I turn?
  4. On to which heading?
  5. How do I know I am there?

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Orientation &mdash; Where am I?

* on heading 300&deg;
* on 360&deg; relative bearing to station
* track 360&deg; to station

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/heading-300-adf-360.png" alt="Heading 300, ADF 360" width="570px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Orientation &mdash; Where am I?

* on heading 135&deg;
* on 225&deg; relative bearing to NDB station
* track 300&deg; to station

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/heading-135-adf-225-transposed.png" alt="Heading 135, ADF 225" width="440px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Orientation &mdash; Where am I?

* on heading 270&deg;
* on 045&deg; relative bearing to NDB station
* track 315&deg; to station

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/heading-270-adf-045-transposed.png" alt="Heading 270, ADF 045" width="440px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Intercept &mdash; Where do I want to be?

* **Intercept**: the heading used to cross the required track to be intercepted
* **Intercept angle**: the angle between the intercept heading and required track
* For intercept angles, use 30&deg;, 45&deg; or 60&deg; for easy calculation

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/aircraft-tracking-ndb-intercept-track.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Aircraft Intercept and Intercept Track" width="380px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Intercept heading &mdash; Where do I want to be?

<p style="font-size: x-large; text-align: center">
  Once on intercept track, adjust for crosswind using the angle/percent rule of thumb
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/crosswind-correction-rule-of-thumb.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="30/45/60 rule of thumb" width="570px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### Intercept heading &mdash; crosswind correction

* For example
  * Track <code>360&deg;</code>
  * Wind: <code>04015KT</code>
  * Heading: <code><strong>005&deg;</strong></code>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/heading-360-adf-360.png" alt="Heading 360, ADF 360" width="570px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### ADF Intercept &mdash; Inbound to Inbound

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/aircraft-tracking-ndb-inbound-inbound-060-030.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Aircraft tracking NDB Inbound to Inbound" width="750px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### ADF Intercept &mdash; Outbound to Outbound

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/aircraft-tracking-ndb-outbound-outbound-180-210.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Aircraft tracking NDB Outbound to Outbound" width="650px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### ADF Intercept &mdash; Inbound to Outbound

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/aircraft-tracking-ndb-inbound-outbound-045-180.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Aircraft tracking NDB Inbound to Outbound" width="650px"/>
</p>

---

## Orientation and Tracking { data-transition="slide-in slide-out" }

###### ADF Intercept &mdash; Outbound to Inbound

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/aircraft-tracking-ndb-outbound-inbound-240-030.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="Aircraft tracking NDB Outbound to Inbound" width="750px"/>
</p>

---

