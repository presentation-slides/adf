## ADF Introduction { data-transition="zoom-in slide-out" }

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Radio Waves

* Radio waves are transmitted from an antenna in all directions at the speed of light
* Frequency is a measure of the number of crests per second
* 1 radio wave crest per second is 1 Hertz
  * One thousand Hertz = 1 Kilohertz = 1 kHz
  * One million Hertz = 1 Megahertz = 1 MHz

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/wave.png" alt="Radio Wave" width="600px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Radio Wave Propagation

* Ground waves are in the frequency range 50-250 kHz
* Ground waves travel through the Earth's surface
* Sky waves are in the frequency range 3-30 MHz
* Sky waves reflect or refract from the ionosphere and return to the ground
* Space waves are in the frequency range 30-300 MHz
* Space waves propagate through the atmosphere to outer space

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/wave-propagation.png" alt="Radio Wave Propagation" width="500px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Equipment

* **NDB:** Non-Directional Beacon, located on the ground
* Operates in the frequency range 200-500 kHz
* May be _pilot-monitored_, requiring a pilot report of faults
  * excessive hum
  * low power
  * loss of ident

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/ndb.png" alt="NDB" width="150px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Equipment

* **ADF:** Automatic Direction Finder, onboard the aircraft
  * **Fixed Card ADF:** A display of 360&deg; compass rose, unable to adjust
  * **Rotating Card ADF:** A display of 360&deg; compass rose, able to be adjusted by the pilot _(most common)_

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/adf-rotating-card.png" alt="ADF Rotating Card" width="350px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Equipment

* Made up of two antennae
  1. Loop: determines the axis of the radio signal
  2. Sense: determines the direction of the radio signal along that axis
* Located under the fuselage of the aircraft

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/adf-antenna.png" alt="ADF Antenna" width="350px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Equipment in the cockpit

* Fixed or Rotating Card ADF
* Tunable ADF Radio Receiver

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/adf/adf-and-radio.png" alt="ADF and Radio" width="350px"/>
</p>

---

## ADF Introduction { data-transition="slide-in slide-out" }

###### Equipment

* This system is very similar to the AM radio in a car
  * A station transmits AM radio waves (NDB)
  * A AM receiver in the car interprets those radio waves (ADF)
  * Difference:
    * the ADF points to the direction of the source of the radio waves
    * the car radio converts the radio signals to sound waves

---

